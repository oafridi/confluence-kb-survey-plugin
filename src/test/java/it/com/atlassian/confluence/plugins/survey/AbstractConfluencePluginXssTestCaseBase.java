package it.com.atlassian.confluence.plugins.survey;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;

import java.util.*;


public class AbstractConfluencePluginXssTestCaseBase extends AbstractConfluencePluginWebTestCase
{
    private List<String> spaceList = new ArrayList<String>();
    private Map<String, Long> pageMap = new HashMap<String, Long>();

    public AbstractConfluencePluginXssTestCaseBase()
    {
        super();
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        restoreData();
        spaceList.add("xss");
        //Create the second space
        SpaceHelper spaceHelper = getSpaceHelper();
        spaceHelper.setKey(getDefaultSpaceKey());
        spaceHelper.setName("<script>alert(document.cookie)</script>");
        spaceHelper.setDescription("<script>alert(document.cookie)</script>");
        spaceHelper.setHomePageId(55556);
        assertTrue(spaceHelper.create());
        pageMap.put("KBarticle", createPage(getDefaultSpaceKey(), "KBarticle", "{kbsurvey}"));
    }

    @Override
    protected void tearDown() throws Exception {
        for (String spaceKey : spaceList)
        {
            deleteSpace(spaceKey);
        }
        super.tearDown();
    }

    protected void deleteSpace(String spaceKey)
    {
        SpaceHelper spaceHelper = getSpaceHelper(spaceKey);
        assertTrue(spaceHelper.delete());
    }
    

    protected long createPage(String spaceKey, String title, String content)
    {
        PageHelper helper = getPageHelper();

        helper.setSpaceKey(spaceKey);
        helper.setParentId(0);
        helper.setTitle(title);
        helper.setContent(content);
        helper.setCreationDate(new Date());
        //helper.setLabels(labels);
        assertTrue(helper.create());

        // return the generated id for the new page
        return helper.getId();
    }

    protected String getDefaultSpaceKey()
    {
        assertTrue(spaceList.size() > 0);
        return spaceList.get(0);
    }
    

    protected void _configureSpace()
    {
        _configureSpace_5();
    }
    
    private void _configureSpace_4()
    {
        assertTrue(spaceList.contains("xss"));
        gotoPage("/display/"+ "xss" + "/KBarticle");
        assertTextPresent("KBarticle");
        clickLink("space-admin-link");
        assertTextPresent("Configure Survey");
        clickLinkWithText("Configure Survey");
    }
    
    private void _configureSpace_5()
    {
        assertTrue(spaceList.contains("xss"));
        gotoPage("/knowledgebase/configuresurvey.action?key=xss");
    }

    protected void _addQuestion(String q)
    {
        setWorkingForm("configurequestions");
        setTextField("additionalQuestion", q);
        clickButton("add");
    }
}
