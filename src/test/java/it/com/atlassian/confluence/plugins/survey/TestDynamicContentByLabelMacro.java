package it.com.atlassian.confluence.plugins.survey;

import com.atlassian.confluence.search.lucene.tasks.ConfluenceIndexTask;
import com.atlassian.confluence.search.lucene.tasks.LuceneConnectionBackedIndexTaskPerformer;
import com.atlassian.confluence.search.IndexTaskPerformer;
import com.atlassian.bonnie.LuceneConnection;

import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;


public class TestDynamicContentByLabelMacro extends AbstractConfluencePluginWebTestCaseBase
{
    private TestIndexQueue indexQueue;
    private LuceneConnectionBackedIndexTaskPerformer indexTaskPerformer;
    private LuceneConnection luceneConnection;
    
   public void testDynamicContentByLabelMacro()
    {
       _configureSpace();
       List<String> labels = new LinkedList<String>();
       labels.add("label1");
       _addPage(getDefaultSpaceKey(),"dynamicContentTestPage","{dynamiccontentbylabel}",labels);
       _addPage(getDefaultSpaceKey(),"dynamicContentTestPage2","{dynamiccontentbylabel}",labels);
       gotoPage("/display/"+ getDefaultSpaceKey() + "/dynamicContentTestPage");

        indexTaskPerformer = new LuceneConnectionBackedIndexTaskPerformer(luceneConnection);
        indexQueue = new TestIndexQueue(indexTaskPerformer);
       //not showing labeled content as it needs to be indexed
       //so we'll just show the title for now
       assertTextPresent("Related Content with Label 'label1'");
    }


    private static class TestIndexQueue
    {
        private final List<ConfluenceIndexTask> tasks = new ArrayList<ConfluenceIndexTask>();
        private final IndexTaskPerformer indexTaskPerformer;

        public TestIndexQueue(IndexTaskPerformer indexTaskPerformer)
        {
            this.indexTaskPerformer = indexTaskPerformer;
        }

        public void add(ConfluenceIndexTask task)
        {
            tasks.add(task);
        }

        public void perform()
        {
            for (ConfluenceIndexTask task : tasks)
            {
                indexTaskPerformer.perform(task);
            }
        }
    }
}
