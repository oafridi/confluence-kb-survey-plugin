package it.com.atlassian.confluence.plugins.survey;

public class TestXssAttack extends AbstractConfluencePluginXssTestCaseBase
{

    public void testKbSurveyDoesNotRenderJavaScript()
    {
        _configureSpace();
        _addQuestion("Question <script>alert(document.cookie)</script>");
        gotoPage("/display/xss/KBarticle");
        assertTextPresent("Question <script>alert(document.cookie)</script>");
    }

    public void testKbSearchDoesNotRenderJavaScript()
    {
        gotoPage("/knowledgebase/kbsearchresults.action?queryString=knowledge&where=xss&spaces=xss&type=pages");
        assertTextPresent("Searching <script>alert(document.cookie)</script>");
    }
}
