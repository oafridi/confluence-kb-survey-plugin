package com.atlassian.confluence.plugins.survey;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.confluence.plugins.knowledgebase.results.KbUnansweredPagesAction;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.opensymphony.webwork.ServletActionContext;
import org.jmock.Mock;
import org.jmock.cglib.MockObjectTestCase;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


public class KbUnansweredPagesActionTest extends MockObjectTestCase
{

    private Settings globalSettings;

    private Mock mockSpaceManager;
    private SpaceManager spaceManager;

    private Mock mockPageManager;
    private PageManager pageManager;

    private Mock mockSurveyFeedbackManager;
    private SurveyFeedbackManager surveyFeedbackManager;

    private Mock mockHttpServletRequest;
    private HttpServletRequest httpServletRequest;

    private Mock mockSettingsManager;
    private SettingsManager settingsManager;

    private KbUnansweredPagesAction kbUnansweredPagesAction;

    protected void setUp() throws Exception
    {
        super.setUp();

        mockSpaceManager = new Mock(SpaceManager.class);
        spaceManager = (SpaceManager) mockSpaceManager.proxy();

        mockPageManager = new Mock(PageManager.class);
        pageManager = (PageManager) mockPageManager.proxy();

        mockSurveyFeedbackManager = new Mock(SurveyFeedbackManager.class);
        surveyFeedbackManager = (SurveyFeedbackManager) mockSurveyFeedbackManager.proxy();

        mockHttpServletRequest = new Mock(HttpServletRequest.class);
        httpServletRequest = (HttpServletRequest) mockHttpServletRequest.proxy();

        mockSettingsManager = new Mock(SettingsManager.class);
        settingsManager = (SettingsManager) mockSettingsManager.proxy();

        ServletActionContext.setRequest(httpServletRequest);

        kbUnansweredPagesAction = new KbUnansweredPagesAction();
        injectKBUnansweredPagesDeps();

    }

    private void injectKBUnansweredPagesDeps()
    {
        kbUnansweredPagesAction.setPageManager(pageManager);
        kbUnansweredPagesAction.setSurveyFeedbackManager(surveyFeedbackManager);

    }

    public void testOneSpaceOnePageResult()
    {
        final Space spaceKB = new Space("KB");
        final Page page2 = new Page();
        page2.setSpace(spaceKB);
        List<Long> pageIdResults = new ArrayList<Long>();

        kbUnansweredPagesAction.setSpace(spaceKB);

        pageIdResults.add(page2.getId());

        mockSurveyFeedbackManager.expects(once()).method("getSurveyConfig").with(eq(spaceKB.getDescription()));       
        mockSurveyFeedbackManager.expects(once()).method("getUnsurveyedPageIdList").with(eq(spaceKB), eq(this.pageManager)).will(returnValue(pageIdResults)); //this should return page2
        mockPageManager.expects(once()).method("getPage").with(eq(page2.getId())).will(returnValue(page2));

        try
        {
            assertEquals("returns SUCCESS","success", kbUnansweredPagesAction.execute());
        }
        catch (Exception e)
        {
            fail("Caught exception " + e.getMessage());
        }

    }

}
