package com.atlassian.confluence.plugins.survey;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.confluence.plugins.knowledgebase.config.SurveyConfig;
import com.atlassian.confluence.plugins.knowledgebase.results.KbRollupAction;
import com.atlassian.confluence.plugins.knowledgebase.results.SurveyResult;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.opensymphony.webwork.ServletActionContext;
import org.jmock.Mock;
import org.jmock.cglib.MockObjectTestCase;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


public class KbRollupActionTest  extends MockObjectTestCase {

    private Settings globalSettings;

    private Mock mockSpaceManager;
    private SpaceManager spaceManager;

    private Mock mockPageManager;
    private PageManager pageManager;

    private Mock mockSurveyFeedbackManager;
    private SurveyFeedbackManager surveyFeedbackManager;

    private Mock mockHttpServletRequest;
    private HttpServletRequest httpServletRequest;

    private Mock mockSettingsManager;
    private SettingsManager settingsManager;

    private KbRollupAction kbRollupAction;

    protected void setUp() throws Exception
    {
        super.setUp();

        mockPageManager = new Mock(PageManager.class);
        pageManager = (PageManager) mockPageManager.proxy();

        mockSurveyFeedbackManager = new Mock(SurveyFeedbackManager.class);
        surveyFeedbackManager = (SurveyFeedbackManager) mockSurveyFeedbackManager.proxy();

        mockHttpServletRequest = new Mock(HttpServletRequest.class);
        httpServletRequest = (HttpServletRequest) mockHttpServletRequest.proxy();

        mockSettingsManager = new Mock(SettingsManager.class);
        settingsManager = (SettingsManager) mockSettingsManager.proxy();

        ServletActionContext.setRequest(httpServletRequest);

        kbRollupAction = new KbRollupAction();
        injectKBRollupActionDeps();

    }

    private void injectKBRollupActionDeps()
    {
        kbRollupAction.setPageManager(pageManager);
        kbRollupAction.setSurveyFeedbackManager(surveyFeedbackManager);
        kbRollupAction.setSettingsManager(settingsManager);
    }

    public void testOneSpaceOnePageResult()
    {
        final Space spaceKB = new Space("KB");
        final Page pageTst = new Page();
        pageTst.setTitle("Article");
        Settings settings = new Settings();
        SurveyConfig surveyConfig = new SurveyConfig();
        List<SurveyResult> pagedResults = new ArrayList<SurveyResult>();
        SurveyResult result = new SurveyResult();

        settings.setBaseUrl("http://localhost:1990/confluence");
        kbRollupAction.setSpace(spaceKB);

        result.setCompositeScore("2");
        pagedResults.add(result);

        mockSettingsManager.expects(once()).method("getGlobalSettings").withNoArguments().will(returnValue(settings));
        mockSurveyFeedbackManager.expects(once()).method("getSurveyConfig").with(eq(spaceKB.getDescription()));

        try
        {
            assertEquals("returns SUCCESS","success", kbRollupAction.execute());
        }
        catch (Exception e)
        {
            fail("Caught exception " + e.getMessage());
        }

    }
}
