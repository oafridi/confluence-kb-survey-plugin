AJS.toInit(function ($) {

    /**
     * function to add a tooltip showing the space name to each drop down list item
     */
    AJS.quicksearch = AJS.quicksearch || {};
    AJS.quicksearch.dropdownPostprocess = function (list) {
         $("a span", list).each(function () {
            var $a = $(this);
            // get the hidden space name property from the span
            var spaceName = AJS.dropDown.getAdditionalPropertyValue($a, "spaceName") || "";

            // we need to go through html node creation so that all encoded symbols(like &gt;) are displayed correctly
            if (spaceName) {
                spaceName = " (" + AJS("i").html(spaceName).text() + ")";
            }

            $a.attr("title", $a.text() + spaceName);
        });
    };

    var types = $(this).find("input.typesJson").val();
    var quicksearch_spaces = $(this).find("input.spacesJson").val();
    var types_json;
    var spaces_json;
    var typesString = "";
    var spacesString = "";
    // determine if the browser has native JSON parser support & create JSON object
    if (typeof (JSON) !== 'undefined' && typeof (JSON.parse) === 'function'){
       types_json = JSON.parse(decodeURIComponent(types).replace(/\+/g, '\u00a0'));
       spaces_json = JSON.parse(decodeURIComponent(quicksearch_spaces).replace(/\+/g, '\u00a0'));
    }
    else{
       types_json = eval('(' + decodeURIComponent(types).replace(/\+/g, '\u00a0') + ')');
       spaces_json = eval('(' + decodeURIComponent(quicksearch_spaces).replace(/\+/g, '\u00a0') + ')');
    }

    for (var i=0; i<types_json.types.length; i++){
       typesString = typesString + "type=" + types_json.types[i].type  + "&";
    }
    if (typesString=="")
    typesString="type=page&type=blogpost&type=attachment&" //default, for Chrome and other browsers

    for (var j=0; j<spaces_json.spaces.length; j++){
        spacesString = spacesString + "key=" + spaces_json.spaces[j].space + "&";
    }
    if (spacesString=="")
    spacesString = "key=" + AJS("i").html(confluenceSpaceKey.attr("content")).text() + "&";

    /**
     * Append the drop down to the form element with the class quick-nav-drop-down
     */
    var quickNavPlacement = function (input) {
        return function (dropDown) {
            input.closest("form").find(".quick-nav-drop-down").append(dropDown);
        };
    };
    var quickSearchQuery = $("#quick-search-query"),
        kbSearchQuery = $("#kb-search-query"),
        confluenceSpaceKey = $("#confluence-space-key");
    quickSearchQuery.quicksearch("/json/contentnamesearch.action", null, {
        dropdownPostprocess : AJS.quicksearch.dropdownPostprocess,
        dropdownPlacement : quickNavPlacement(quickSearchQuery)
    });
    if (kbSearchQuery.length && confluenceSpaceKey.length) {
        kbSearchQuery.quicksearch("/jsonkb/kbsearch.action?"+typesString+spacesString+"spaceKey=" +
                AJS("i").html(confluenceSpaceKey.attr("content")).text(), null, {
            dropdownPostprocess : AJS.quicksearch.dropdownPostprocess,
            dropdownPlacement : quickNavPlacement(kbSearchQuery)
        });
    };
});


