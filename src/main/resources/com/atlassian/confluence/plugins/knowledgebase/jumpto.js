AJS.$(function()
{
    var $survey = AJS.$("form.survey");
    $survey.submit(function(e){
        e.preventDefault();
    }); // stop the form from submitting

    $survey.find("input:radio").click(function(){
	var q = jQuery.parseQuery();
        var $radio = AJS.$(this);
        AJS.$.post($survey.attr("action"), { questionId: $radio.attr("name"), value: $radio.val(), utmSource: q.utm_source, utmMedium: q.utm_medium, utmContent: q.utm_content }, function(xmlresponse){
            $radio.parent().hide('slow');
           
            var $responseMessage = AJS.$(xmlresponse).find("message").text();
            var $responseVerdict = AJS.$(xmlresponse).find("result").text();

            if ($responseVerdict == "true"){
                  $radio.closest('table').append($responseMessage);
            }
        });
    });
});

