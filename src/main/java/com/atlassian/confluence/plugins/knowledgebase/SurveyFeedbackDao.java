package com.atlassian.confluence.plugins.knowledgebase;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.knowledgebase.results.SurveyGaResult;
import com.atlassian.confluence.plugins.knowledgebase.results.SurveyResult;
import com.atlassian.confluence.spaces.Space;

import java.util.List;

public interface SurveyFeedbackDao {

    public int getSpaceBallotCount(Space space);

    public String getSpacePrimaryYesNoTotal(String questionId);

    public List<String> getPageIdsSortedByCompositeScore(String spaceKey, Integer max, Integer yesIncrement, Integer noDecrement);

    public List<Long> getUnsurveyedPages(Space space, PageManager pageManager);

    public int getNumberOfSurveyedPages(String spaceKey);

    public List<SurveyResult> getPagedResults(String order, String spaceKey, int offset, int max, Integer yesIncrement, Integer noDecrement);
    public List<SurveyGaResult> getPagedResults(String gaTag, String order, String spaceKey, int offset, int max, Integer yesIncrement, Integer noDecrement);
}
