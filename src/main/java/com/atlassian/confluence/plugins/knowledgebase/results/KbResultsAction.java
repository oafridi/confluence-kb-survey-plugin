package com.atlassian.confluence.plugins.knowledgebase.results;

import bucket.core.actions.PaginationSupport;
import com.atlassian.confluence.core.DefaultListBuilder;
import com.atlassian.confluence.core.ListBuilder;
import com.atlassian.confluence.core.ListBuilderCallback;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.knowledgebase.Constants;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.confluence.plugins.knowledgebase.config.SurveyConfig;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.AbstractSpaceAdminAction;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import java.util.List;


/**
 * Space admin web item to display total ballots, composite score, and per-question results
 */

@WebSudoRequired
public class KbResultsAction extends AbstractSpaceAdminAction {

    private PageManager pageManager;    
    private List<SurveyResult> resultsList;
    private int startIndex = 0;
    private String sort = "composite_desc";
    private SurveyConfig surveyConfig;
    private String baseUrl;
    private int resultSize;
    private PaginationSupport paginationSupport;
    private SurveyFeedbackManager surveyFeedbackManager;


    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }

    public String execute() throws Exception
    {
        Space space = this.getSpace();
        baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
        surveyConfig = surveyFeedbackManager.getSurveyConfig(space.getDescription());

        ListBuilder<SurveyResult> rankedResultsList = getRankedResults(sort,space.getKey());
        resultsList = rankedResultsList.getPage(startIndex, Constants.MAX_RESULTS_PER_PAGE);
        resultSize = rankedResultsList.getAvailableSize();
        paginationSupport = new PaginationSupport(Constants.MAX_RESULTS_PER_PAGE);
        paginationSupport.setStartIndex(startIndex);
        paginationSupport.setTotal(resultSize);

        return SUCCESS;
    }

    public ListBuilder<SurveyResult> getRankedResults(final String order, final String spaceKey)
    {
        return DefaultListBuilder.newInstance(new ListBuilderCallback<SurveyResult>()
        {
            public List<SurveyResult> getElements(int offset, int maxResults)
            {
                return surveyFeedbackManager.getPagedResults(order, spaceKey, offset, maxResults);
            }

            public int getAvailableSize()
            {
                return surveyFeedbackManager.getNumberOfSurveyedPages(spaceKey);
            }
        });
    }

    public SurveyConfig getSurveyConfig()
    {
        return surveyConfig;
    }

    public void setPageManager (PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public PaginationSupport getPaginationSupport()
	{
		return paginationSupport;
	}

    public int getResultSize()
    {
        return resultSize;
    }

    public String getPageTitle(SurveyResult surveyResult)
    {
        return pageManager.getPage(Long.parseLong(surveyResult.getPageId())).getTitle();
    }

    public String getSort()
    {
        return sort.toLowerCase();
    }

    public List<SurveyResult> getResultsList()
    {
        return resultsList;
    }
    
    public String getBaseUrl()
    {
        return baseUrl;
    }

    public boolean hasPageResult(AbstractPage page)
    {
        return surveyFeedbackManager.hasPageResult(page,spaceManager.getSpaceFromPageId(page.getId()));
    }

    public String getYesNoPercentage(String page,String questionNumber)
    {
        return surveyFeedbackManager.getYesNoPercentage(pageManager.getPage(Long.parseLong(page)), questionNumber);
    }

    public String getNumberOfSurveys(AbstractPage page)
    {
        return surveyFeedbackManager.getNumberOfSurveys(page, spaceManager.getSpaceFromPageId(page.getId()));
    }
    
    public String getCompositeScore(String page)
    {
        return surveyFeedbackManager.getCompositeScore(pageManager.getPage(Long.parseLong(page)));
    }

    public void setSort(String sort)
    {
        this.sort = sort;
    }

    public void setStartIndex(String startIndex)
    {
        this.startIndex = Integer.parseInt(startIndex);
    }
}

