package com.atlassian.confluence.plugins.knowledgebase;

import java.util.Date;

import net.java.ao.Entity;
import net.java.ao.schema.*;

public interface Response extends Entity {
	@AutoIncrement
    @NotNull
    @PrimaryKey("ID")
	public int getID();

	@NotNull
	public long getPageId();
	public void setPageId(long pageId);
	
	@NotNull
	public String getSpaceKey();
	public void setSpaceKey(String spaceKey);
	
	@NotNull
	public String getQuestionId();
	public void setQuestionId(String questionId);
	
	@NotNull
	public boolean getAnswer();
	public void setAnswer(boolean answer);
	
	public String getUtmSource();
	public void setUtmSource(String utmSource);
	
	public String getUtmMedium();
	public void setUtmMedium(String utmMedium);
	
	public String getUtmContent();
	public void setUtmContent(String utmContent);
	
	public Date getDate();
	public void setDate(Date date);
}
