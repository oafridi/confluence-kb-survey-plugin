package com.atlassian.confluence.plugins.knowledgebase.config;

import com.atlassian.confluence.spaces.actions.SpaceAware;
import com.atlassian.confluence.spaces.actions.AbstractSpaceAdminAction;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;

/**
 * Configure the feedback on  space level
 */
@WebSudoRequired
public class ConfigureSurveyAction extends AbstractSpaceAdminAction implements SpaceAware
{
    private SurveyConfig surveyConfig;
    private SurveyFeedbackManager surveyFeedbackManager;

    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }

    @com.atlassian.xwork.RequireSecurityToken(true)
    public String execute() throws Exception
    {
        SpaceDescription spaceDesc = this.getSpace().getDescription();
        surveyConfig = surveyFeedbackManager.getSurveyConfig(spaceDesc);
        surveyFeedbackManager.setSurveyConfig(spaceDesc,surveyConfig);
        return super.doDefault();
    }

    public SurveyConfig getSurveyConfig()
    {
        return surveyConfig;
    }
}


