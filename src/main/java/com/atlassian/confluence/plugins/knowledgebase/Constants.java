package com.atlassian.confluence.plugins.knowledgebase;

@SuppressWarnings("unused")
public final class Constants
{
	public static final int BUILD_NUMBER = 1; // Change only when data model changes
	public static final String PLUGIN_KEY = "confluence.plugins.knowledgebase.survey";
	public static final String UPGRADE_DESC = "Migrates previous content to Active Objects tables";
	public static final String GA_NULL_VALUE = "Undefined";
	
    public static final int DEFAULT_YES_INCREMENT = 2;
    public static final int DEFAULT_NO_DECREMENT = -1;
    public static final int DEFAULT_BOOST_AMPLIFIER = 3;
    public static final int DEFAULT_BOOST_MAXIMUM = 100;
    public static final int DEFAULT_BOOST_MINIMUM = 40;

    public static final String SETTING_PREFIX = "com.atlassian.confluence.plugins.knowledgebase.";
    public static final String GLOBAL_SPACE_KEY = "GLOBAL";
    public static final String YES_INCREMENT_KEY = SETTING_PREFIX + "yesincrement";
    public static final String NO_DECREMENT_KEY = SETTING_PREFIX + "nodecrement";
    public static final String BOOST_AMPLIFIER_KEY = SETTING_PREFIX + "boostamplifier";
    public static final String BOOST_MAXIMUM_KEY = SETTING_PREFIX + "boostmaximum";
    public static final String BOOST_MINIMUM_KEY = SETTING_PREFIX + "boostminimum";

    public static final String CONFIG = SETTING_PREFIX + "kbsurvey.config";
    public static final String COMPOSITE_SCORE = SETTING_PREFIX + "kbsurvey.results.compositescore.";
    public static final String TOTAL_RESULTS = SETTING_PREFIX + "kbsurvey.results.totalsurveys.";
    public static final String RESULTS = SETTING_PREFIX + "kbsurvey.results.";
    public static final String MARKER = SETTING_PREFIX + "kbsurvey.results.survey.placeholder";
    public static final String BALLOT = SETTING_PREFIX + "kbsurvey.results.ballot.";
    public static final int MAX_STORED_RESULTS = 32;

    public static final int MAX_RESULTS_PER_PAGE=15;

    private Constants()
    {
        throw new AssertionError("Do not instantiate the Constants class");
    }
}
