package com.atlassian.confluence.plugins.knowledgebase;

import static com.atlassian.confluence.plugins.knowledgebase.Constants.*;
import net.java.ao.DBParam;
import net.java.ao.Query;

import org.apache.log4j.Logger;
import com.atlassian.activeobjects.external.*;

public class DefaultSurveySettingsManager implements SurveySettingsManager
{
    private static final Logger log = Logger.getLogger(SurveySettingsManager.class);
    private ActiveObjects ao;
    
    public DefaultSurveySettingsManager()
    {
    	// Do nothing
    }
    
    public void setActiveObjects(final ActiveObjects ao)
    {
    	this.ao = ao;
    }

    public Integer getYesIncrement()
    {
        Integer result = getSetting(YES_INCREMENT_KEY);
        return result != null ? result : defaultYesIncrement();
    }

    private int defaultYesIncrement()
    {
        setYesIncrememt(DEFAULT_YES_INCREMENT);
        return DEFAULT_YES_INCREMENT;
    }

    public void setYesIncrememt(Integer percentage)
    {
        store(YES_INCREMENT_KEY, percentage);
    }

    public Integer getNoDecrement()
    {
        Integer result = getSetting(NO_DECREMENT_KEY);
        return result != null ? result : defaultNoDecrement();
    }

    private int defaultNoDecrement()
    {
        setNoDecrememt(DEFAULT_NO_DECREMENT);
        return DEFAULT_NO_DECREMENT;
    }

    public void setNoDecrememt(int percentage)
    {
        store(NO_DECREMENT_KEY, percentage);
    }

    public Integer getBoostAmplifier()
    {
        Integer result = getSetting(BOOST_AMPLIFIER_KEY);
        return result != null ? result : defaultBoostAmplifier();
    }

    private int defaultBoostAmplifier()
    {
        setBoostAmplifier(DEFAULT_BOOST_AMPLIFIER);
        return DEFAULT_BOOST_AMPLIFIER;
    }

    public void setBoostAmplifier(int percentage)
    {
        store(BOOST_AMPLIFIER_KEY, percentage);
    }

    public Integer getBoostMaximum()
    {
        Integer result = getSetting(BOOST_MAXIMUM_KEY);
        return result != null ? result : defaultBoostMaximum();
    }

    private int defaultBoostMaximum()
    {
        setBoostMaximum(DEFAULT_BOOST_MAXIMUM);
        return DEFAULT_BOOST_MAXIMUM;
    }

    public void setBoostMaximum(int percentage)
    {
        store(BOOST_MAXIMUM_KEY, percentage);
    }

    public Integer getBoostMinimum()
    {
        Integer result = getSetting(BOOST_MINIMUM_KEY);
        return result != null ? result : defaultBoostMinimum();
    }

    private Integer defaultBoostMinimum()
    {
        setBoostMinimum(DEFAULT_BOOST_MINIMUM);
        return DEFAULT_BOOST_MINIMUM;
    }

    public void setBoostMinimum(int percentage)
    {
        store(BOOST_MINIMUM_KEY, percentage);
    }

    private Integer getSetting(String key)
    {
    	Settings[] ao_current_settings = ao.find(Settings.class, Query.select().where("KEY = ? and SPACE = ?", key, GLOBAL_SPACE_KEY));
        if (ao_current_settings.length > 0)
        	// There should be only one result
        	return ao_current_settings[0].getValue();
        else
        	return null;
    }

    private void store(String key, Integer value)
    {
    	// AO_Settings ao_settings = ao.get(AO_Settings.class, id);
    	Settings[] ao_current_settings = ao.find(Settings.class, Query.select().where("KEY = ? and SPACE = ?", key, GLOBAL_SPACE_KEY));
    	Settings ao_settings = (ao_current_settings.length > 0)? ao_current_settings[0] : null;
    	if (ao_settings == null)
    		// Setting does not exist in DB. Create a new entry.
    		ao.create(Settings.class, new DBParam("SPACE", GLOBAL_SPACE_KEY), new DBParam("KEY", key), new DBParam("VALUE", value));
    	else {
    		// Setting already exist. Update current entry.
    		ao_settings.setKey(key);
    		ao_settings.setSpace(GLOBAL_SPACE_KEY);
    		ao_settings.setValue(value);  
    		ao_settings.save();
    	}
    }
}
