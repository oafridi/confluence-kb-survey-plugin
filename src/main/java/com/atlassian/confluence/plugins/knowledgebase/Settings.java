package com.atlassian.confluence.plugins.knowledgebase;

import net.java.ao.RawEntity;
import net.java.ao.schema.*;
import net.java.ao.Entity;

public interface Settings extends RawEntity<Integer> {
	@PrimaryKey
	@AutoIncrement
	public Integer getId();

	@NotNull
	public String getSpace();
	public void setSpace(String key);
	
	@NotNull
	public String getKey();
	public void setKey(String key);

	@NotNull
	public Integer getValue();
	public void setValue(Integer value);
}
