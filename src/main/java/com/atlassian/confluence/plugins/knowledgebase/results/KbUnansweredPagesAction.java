package com.atlassian.confluence.plugins.knowledgebase.results;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.AbstractSpaceAdminAction;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.confluence.plugins.knowledgebase.config.SurveyConfig;
import com.atlassian.confluence.core.*;

import com.atlassian.confluence.plugins.knowledgebase.Constants;

import java.util.List;
import java.util.LinkedList;
import java.util.Collections;

import bucket.core.actions.PaginationSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.apache.log4j.Logger;

@WebSudoRequired
public class KbUnansweredPagesAction extends AbstractSpaceAdminAction
{
    private PageManager pageManager;
    private List<Page> pageList;
    private int startIndex = 0;
    private SurveyConfig surveyConfig;
    private PaginationSupport paginationSupport;
    private SurveyFeedbackManager surveyFeedbackManager;

    private static Logger log = Logger.getLogger(KbUnansweredPagesAction.class);


    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }

    public String execute() throws Exception
    {
        final Space space = this.getSpace();
        surveyConfig = surveyFeedbackManager.getSurveyConfig(space.getDescription());
        List<Page> fullUnansweredPageList = new LinkedList<Page>();
        /*
        finding it more effective to obtain a unsurveyed page (id) list and
        building a list of unsurveyed pages from it 
         */
        long spaceId = space.getId();
        List<Long> unsurveyedPageIds = surveyFeedbackManager.getUnsurveyedPageIdList(space, pageManager);
        for (Long unsurveyedPageId : unsurveyedPageIds)
        {
             fullUnansweredPageList.add(pageManager.getPage(unsurveyedPageId));
        }

        Collections.sort(fullUnansweredPageList);
        pageList = getUnansweredResults(fullUnansweredPageList).getPage(startIndex,Constants.MAX_RESULTS_PER_PAGE);
        paginationSupport = new PaginationSupport(Constants.MAX_RESULTS_PER_PAGE);
        paginationSupport.setStartIndex(startIndex);
        paginationSupport.setTotal(fullUnansweredPageList.size());

        return SUCCESS;
    }

    public ListBuilder<Page> getUnansweredResults(final List<Page> answeredPageList)
    {
        return DefaultListBuilder.newInstance(new ListBuilderCallback<Page>()
        {
            public List<Page> getElements(int offset, int maxResults)
            {
                if (offset+maxResults <= answeredPageList.size())
                   return answeredPageList.subList(offset,offset+maxResults);
                return answeredPageList.subList(offset,answeredPageList.size());
            }

            public int getAvailableSize()
            {
                return answeredPageList.size();
            }
        });

    }

    public PaginationSupport getPaginationSupport()
	{
		return paginationSupport;
	}

    public SurveyConfig getSurveyConfig()
    {
        return surveyConfig;
    }

    public List<Page> getPageList()
    {
        return pageList;
    }

    public void setPageManager (PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public void setStartIndex(int startIndex)
    {
        this.startIndex = startIndex;
    }
}