package com.atlassian.confluence.plugins.knowledgebase;


import com.atlassian.confluence.core.ConfluenceActionSupport;

public class ConfigureAction extends ConfluenceActionSupport
{
    private SurveySettingsManager surveySettingsManager;
    private int yesIncrement;
    private int noDecrement;
    private int boostAmplifier;
    private int boostMaximum;
    private int boostMinimum;

    @Override
    public String execute()
    {
        if (yesIncrement == 0)
        {
            return INPUT;
        }

        surveySettingsManager.setYesIncrememt(yesIncrement);
        surveySettingsManager.setNoDecrememt(noDecrement);
        surveySettingsManager.setBoostAmplifier(boostAmplifier);
        surveySettingsManager.setBoostMaximum(boostMaximum);
        surveySettingsManager.setBoostMinimum(boostMinimum);

        return SUCCESS;
    }

    public void setSurveySettingsManager(SurveySettingsManager surveySettingsManager)
    {
        this.surveySettingsManager = surveySettingsManager;
    }

    public void setNoDecrement(int decrement)
    {
        this.noDecrement = decrement;
    }

    public int getNoDecrement()
    {
        return surveySettingsManager.getNoDecrement();
    }

    public void setYesIncrement(int increment)
    {
        this.yesIncrement = increment;
    }

    public int getYesIncrement()
    {
        return surveySettingsManager.getYesIncrement();
    }

    public void setBoostAmplifier(int amplifier)
    {
        this.boostAmplifier = amplifier;
    }

    public int getBoostAmplifier()
    {
        return surveySettingsManager.getBoostAmplifier();
    }

    public void setBoostMaximum(int boostMaximum)
    {
        this.boostMaximum = boostMaximum;
    }

    public int getBoostMaximum()
    {
        return surveySettingsManager.getBoostMaximum();
    }

    public void setBoostMinimum(int boostMinimum)
    {
        this.boostMinimum = boostMinimum;
    }

    public int getBoostMinimum()
    {
        return surveySettingsManager.getBoostMinimum();
    }
}
