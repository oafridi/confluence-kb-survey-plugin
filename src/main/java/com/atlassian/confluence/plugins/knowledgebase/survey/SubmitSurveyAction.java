package com.atlassian.confluence.plugins.knowledgebase.survey;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.actions.PageAware;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.confluence.plugins.knowledgebase.config.SurveyConfig;
import com.atlassian.confluence.plugins.knowledgebase.config.SurveyQuestion;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.search.ConfluenceIndexer;
import com.opensymphony.webwork.interceptor.ServletRequestAware;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

public class SubmitSurveyAction extends ConfluenceActionSupport implements ServletRequestAware, PageAware
{
    private HttpServletRequest request;
    private AbstractPage page;
    private String redirectURL;
    private SpaceManager spaceManager;
    private static final Logger log = Logger.getLogger(SubmitSurveyAction.class);
    private ConfluenceIndexer indexer;

    // variables for ajax call
    private String message;
    private boolean result;
    private SurveyConfig surveyConfig;
    private HttpSession session;
    private SurveyFeedbackManager surveyFeedbackManager;

    private String questionId;
    private String value;
    private String utmSource;
    private String utmMedium;
    private String utmContent;

    private static final String KB_SESSION_ATT = "kb-survey";

    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }

    public String execute() throws Exception
    {
        session = request.getSession(true);
        result = false;

        if (value != null && questionId != null)
        {
            //Add survey result to score in database
            surveyFeedbackManager.addSurveyResult(page, questionId, value.equals("Yes"), spaceManager.getSpaceFromPageId(page.getId()), utmSource, utmMedium, utmContent);

            //Update composite score in index, set it thru KbSearchExtractor
            indexer.reIndex(page);

            session.setAttribute(KbHttpSession.getSessionAttribute(page, questionId), "answered");

            surveyConfig = surveyFeedbackManager.getSurveyConfig(page.getSpace().getDescription());
            result = checkComplete();
            message = surveyConfig.getResponse(session);

        }
        return SUCCESS;
    }


    private boolean checkComplete()
    {

//        String pageId = page.getIdAsString();
//        String sessionAtt = pageId + surveyConfig.getPrimaryQuestionId();
        Object answered = session.getAttribute(KbHttpSession.getSessionAttribute(page,surveyConfig.getPrimaryQuestionId()));
        if (answered == null)
        {
            return false;
        }

        for (SurveyQuestion s : surveyConfig.getActiveQuestionList())
        {
            String sessionAtt = KbHttpSession.getSessionAttribute(page, s.getId()) ;
            log.info("looking up " + sessionAtt);
            answered = session.getAttribute(sessionAtt);
            if( answered == null)
            {
                return false;
            }
        }
        return true;

    }


    public String getRedirectURL()
    {
        return redirectURL;
    }

    public void setIndexer(ConfluenceIndexer indexer)
    {
        this.indexer = indexer;
    }

    public void setSpaceManager (SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }


    public void setServletRequest(HttpServletRequest httpServletRequest)
    {
        this.request = httpServletRequest;
    }

    public AbstractPage getPage()
    {
        return page;
    }

    public void setPage(AbstractPage page)
    {
        this.page = page;
        log.debug(page);
    }

    public boolean isViewPermissionRequired()
    {
        return true;
    }

    public boolean isLatestVersionRequired()
    {
        return true;
    }

    public boolean isPageRequired()
    {
        return true;
    }

    public boolean isResult()
    {
        return result;
    }

    public void setResult(boolean result)
    {
        this.result = result;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public void setQuestionId(String questionId)
    {
        this.questionId = questionId;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
    public void setUtmSource(String value)
    {
        this.utmSource = value;
    }
    public void setUtmMedium(String value)
    {
        this.utmMedium = value;
    }
    public void setUtmContent(String value)
    {
        this.utmContent = value;
    }
}
