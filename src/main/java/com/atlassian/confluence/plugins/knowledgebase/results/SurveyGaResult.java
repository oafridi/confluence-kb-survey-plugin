package com.atlassian.confluence.plugins.knowledgebase.results;


public final class SurveyGaResult
{
    private String gaTag;
    private String gaValue;
    private String compositeScore;
    private String totalSurveys;

    public void setGaTag(String gaTag)
    {
        this.gaTag = gaTag;
    }
    
    public String getGaTag()
    {
        return gaTag;
    }

    public void setGaValue(String gaValue)
    {
        this.gaValue = (gaValue == null)? "NULL" :gaValue;
    }
    
    public String getGaValue()
    {
        return gaValue;
    }
    
    public void setCompositeScore(String compositeScore)
    {
        this.compositeScore = compositeScore;
    }

    public String getCompositeScore()
    {
        return compositeScore;
    }

    public void setTotalSurveys(String totalSurveys)
    {
        this.totalSurveys = totalSurveys;
    }

    public String getTotalSurveys()
    {
        return totalSurveys;
    }
}
