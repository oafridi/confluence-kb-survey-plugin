package com.atlassian.confluence.plugins.knowledgebase.results;


public final class SurveyResult
{
    private String pageId;
    private String compositeScore;
    private String totalSurveys;

    public void setPageId(String pageId)
    {
        this.pageId = pageId;
    }

    public String getPageId()
    {
        return pageId;
    }
    public void setCompositeScore(String compositeScore)
    {
        this.compositeScore = compositeScore;
    }

    public String getCompositeScore()
    {
        return compositeScore;
    }

    public void setTotalSurveys(String totalSurveys)
    {
        this.totalSurveys = totalSurveys;
    }

    public String getTotalSurveys()
    {
        return totalSurveys;
    }
}
