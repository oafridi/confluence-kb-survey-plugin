package com.atlassian.confluence.plugins.knowledgebase.results;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TopArticlesMacro extends BaseMacro
{
    private PageManager pageManager;
    private static final int DEFAULT_NUMBER = 10;
    private static final int MAX_ARTICLES = 50; // you can't want more than that right?
    private SurveyFeedbackManager surveyFeedbackManager;
    private ConfluenceActionSupport confluenceActionSupport;

    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public ConfluenceActionSupport getConfluenceActionSupport()
    {
        if (null == confluenceActionSupport)
        {
            confluenceActionSupport = GeneralUtil.newWiredConfluenceActionSupport();
        }
        return confluenceActionSupport;
    }

    public String execute(Map params, String body, RenderContext renderContext) throws MacroException
    {

        PageContext pageContext;
        if (renderContext instanceof PageContext)
        {
            pageContext = (PageContext) renderContext;
        }
        else
        {
            throw new MacroException(ConfluenceActionSupport.getTextStatic("kbsearch.error.macro-works-in-only-page"));
        }
        try
        {
            Map<String, String> typeSafeParams = (Map<String, String>) params;
            //Get space parameter
            String spaceParam;
            if (typeSafeParams.containsKey("space"))
            {
                spaceParam = typeSafeParams.get("space");
            }
            else
            {
                spaceParam = pageContext.getSpaceKey();
            }
            //Get number parameter
            final Map<String,Object> contextMap = MacroUtils.defaultVelocityContext();
            final String maxResults = typeSafeParams.get("0");
            int max = DEFAULT_NUMBER;
            if (maxResults != null)
            {
                max = Integer.parseInt(maxResults);
            }
            if (max > MAX_ARTICLES)
            {
                max = MAX_ARTICLES;
            }
            //Then get composite scores and sort them
            List<String> pageIdList = surveyFeedbackManager.getPageIdsSortedByCompositeScore(spaceParam, max);
            List<Page> pageList = new LinkedList<Page>();
            //convert Strings to Pages
            for (String pageString : pageIdList)
            {
                pageList.add(pageManager.getPage(Long.parseLong(pageString)));
            }
            contextMap.put("pagelist", pageList);
            contextMap.put("surveyfeedbackmanager", surveyFeedbackManager);
            return VelocityUtils.getRenderedTemplate("/com/atlassian/confluence/plugins/knowledgebase/results/toparticles.vm", contextMap);
        }
        catch (NumberFormatException e)
        {
            return "Error parsing number parameter in toparticles macro.";
        }
    }

    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public String getCompositeScore(AbstractPage page)
    {
        return surveyFeedbackManager.getCompositeScore(page);
    }
}
