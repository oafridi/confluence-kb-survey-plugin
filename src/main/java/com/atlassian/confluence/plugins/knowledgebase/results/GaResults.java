package com.atlassian.confluence.plugins.knowledgebase.results;

import bucket.core.actions.PaginationSupport;
import com.atlassian.confluence.core.DefaultListBuilder;
import com.atlassian.confluence.core.ListBuilder;
import com.atlassian.confluence.core.ListBuilderCallback;
import com.atlassian.confluence.plugins.knowledgebase.Constants;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.confluence.plugins.knowledgebase.config.SurveyConfig;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.AbstractSpaceAdminAction;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import java.util.List;


/**
 * Space admin web item to display total ballots, composite score, and per-question results
 */

@WebSudoRequired
public class GaResults extends AbstractSpaceAdminAction {
    
    private List<SurveyGaResult> resultsList;
    private int startIndex = 0;
    private String sort = "composite_desc";
    private String group = "source";
    private SurveyConfig surveyConfig;
    private String baseUrl;
    private int resultSize;
    private PaginationSupport paginationSupport;
    private SurveyFeedbackManager surveyFeedbackManager;


    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }

    public String execute() throws Exception
    {
        Space space = this.getSpace();
        baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
        surveyConfig = surveyFeedbackManager.getSurveyConfig(space.getDescription());

        ListBuilder<SurveyGaResult> rankedResultsList = getRankedResults(sort,space.getKey());
        resultsList = rankedResultsList.getPage(startIndex, Constants.MAX_RESULTS_PER_PAGE);
        resultSize = rankedResultsList.getAvailableSize();
        paginationSupport = new PaginationSupport(Constants.MAX_RESULTS_PER_PAGE);
        paginationSupport.setStartIndex(startIndex);
        paginationSupport.setTotal(resultSize);

        return SUCCESS;
    }

    public ListBuilder<SurveyGaResult> getRankedResults(final String order, final String spaceKey)
    {
        return DefaultListBuilder.newInstance(new ListBuilderCallback<SurveyGaResult>()
        {
            public List<SurveyGaResult> getElements(int offset, int maxResults)
            {
                return surveyFeedbackManager.getPagedResults(getGaTag(group), order, spaceKey, offset, maxResults);
            }

            public int getAvailableSize()
            {
                return surveyFeedbackManager.getNumberOfGaValues(getGaTag(group), spaceKey);
            }
        });
    }

    public SurveyConfig getSurveyConfig()
    {
        return surveyConfig;
    }

    public PaginationSupport getPaginationSupport()
	{
		return paginationSupport;
	}

    public int getResultSize()
    {
        return resultSize;
    }

    public String getSort()
    {
        return sort.toLowerCase();
    }
    public String getGroup()
    {
        return group.toLowerCase();
    }

    public List<SurveyGaResult> getResultsList()
    {
        return resultsList;
    }
    
    public String getBaseUrl()
    {
        return baseUrl;
    }

    /*public boolean hasPageResult(AbstractPage page)
    {
        return surveyFeedbackManager.hasGaResult(page,spaceManager.getSpaceFromPageId(page.getId()));
    }*/

    public String getYesNoPercentage(String gaValue,String spaceKey, String questionNumber)
    {
        return surveyFeedbackManager.getYesNoPercentage(getGaTag(group), gaValue, spaceKey, questionNumber);
    }

    public String getNumberOfSurveys(String gaValue,String spaceKey) // TODO: Needed?
    {
        return surveyFeedbackManager.getNumberOfSurveys(getGaTag(group), gaValue, spaceKey);
    }
    
    public String getCompositeScore(String gaValue,String spaceKey) // TODO: Needed?
    {
        return surveyFeedbackManager.getCompositeScore(getGaTag(group), gaValue, spaceKey);
    }

    public void setSort(String sort)
    {
        this.sort = sort;
    }
    
    public void setGroup(String group)
    {
        this.group = group;
    }

    public void setStartIndex(String startIndex)
    {
        this.startIndex = Integer.parseInt(startIndex);
    }
    private static String getGaTag(String tag) {
    		return "UTM_" + tag.toUpperCase();
    }
}

