package com.atlassian.confluence.plugins.knowledgebase;

public interface SurveySettingsManager
{
    public Integer getYesIncrement();
    public void setYesIncrememt(Integer percentage);

    public Integer getNoDecrement();
    public void setNoDecrememt(int percentage);

    public Integer getBoostAmplifier();
    public void setBoostAmplifier(int percentage);

    public Integer getBoostMaximum();
    public void setBoostMaximum(int percentage);

    public Integer getBoostMinimum();
    public void setBoostMinimum(int percentage);

}
