package com.atlassian.confluence.plugins.knowledgebase.search;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.search.actions.ContentNameSearchAction;
import com.atlassian.confluence.search.actions.json.ContentNameMatch;
import com.atlassian.confluence.search.actions.json.ContentNameSearchResult;
import com.atlassian.confluence.search.contentnames.Category;
import com.atlassian.confluence.search.contentnames.ContentNameSearcher;
import com.atlassian.confluence.search.contentnames.QueryToken;
import com.atlassian.confluence.search.contentnames.QueryTokenizer;
import com.atlassian.confluence.search.contentnames.ResultTemplate;
import com.atlassian.confluence.search.contentnames.SearchResult;
import com.atlassian.confluence.search.contentnames.SemaphoreHolder;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.actions.ContentTypesDisplayMapper;
import com.opensymphony.webwork.interceptor.ServletRequestAware;
import com.opensymphony.xwork.Action;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class KbSearchAction extends ContentNameSearchAction implements Beanable, ServletRequestAware
{
	static final String SEARCH_FOR_CSS_CLASS = "search-for";


    private static Logger log = Logger.getLogger(KbSearchAction.class);

	/** The ordering of categories in the created <code>ContentNameSearchResult</code> */
	private static List<Category> CATEGORY_ORDERING = new ArrayList<Category>(Arrays.asList(Category.CONTENT, Category.ATTACHMENTS, Category.PEOPLE, Category.SPACES));

	private ContentTypesDisplayMapper contentTypesDisplayMapper;
	private ContentNameSearcher contentNameSearcher;
	private QueryTokenizer contentNameQueryTokenizer;
    private SemaphoreHolder contentNameSearchSemaphoreHolder;
    private SettingsManager settingsManager;
    private SpaceManager spaceManager;
    private String query;
    private List<QueryToken> queryTokens;
    private String[] types;
    private String[] keys;
    private String spaceKey;

    private ContentNameSearchResult result;
    /**
     * Maximum time to wait to acquire a permit.
     */
    private static final int MAX_PERMIT_ACQUIRE_TIME_MILLIS = 500;
    private HttpServletRequest servletRequest;

    /**
	 * <p>
	 * Perform the search and create the <code>ContentNameSearchResult</code>. The individual
	 * <code>ContentNameMatch</code> objects within the result will be ordered and grouped, so you can count on getting
	 * results ordered like -
	 * <ul>
	 * <li>all page matches
	 * <li>all blog matches
	 * <li>all attachment matches
	 * <li>all user matches
	 * <li>all space matches
	 * </ul>
	 *
	 * If a particular category has no matches then it will not be included in the result.
	 * </p>
	 */

	public String execute() throws Exception
	{
        boolean permitAcquired;
        result = new ContentNameSearchResult(query);

        if (!settingsManager.getGlobalSettings().isEnableQuickNav())
        {
            result.setStatusMessage(getText("quick.nav.disabled"));
            return SUCCESS;
        }

        final Semaphore permits = contentNameSearchSemaphoreHolder.getSemaphore();

        try
        {
            permitAcquired = permits.tryAcquire(MAX_PERMIT_ACQUIRE_TIME_MILLIS, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException e)
        {
            result.setStatusMessage(getText("quick.nav.cancelled"));
            Thread.currentThread().interrupt();
            return SUCCESS;
        }

        if (permitAcquired)
        {
            try
            {
                long startTime = 0;
                if (log.isDebugEnabled())
                     startTime = System.currentTimeMillis();

                queryTokens = contentNameQueryTokenizer.tokenize(query);

                result.setQueryTokens(queryTokens);

                if (queryTokens == null) //JL changed from Collections.isEmpty(queryTokens)
                {
                    result.setStatusMessage(getText("contentnamesearch.invalid.query"));
                }
                else
                {
                    ResultTemplate resultTemplate = ResultTemplate.DEFAULT;

                    if (!ArrayUtils.isEmpty(types))
                    {
                        resultTemplate = new ResultTemplate()
                        {{
                            for (String type : types)
                            {
                                if (StringUtils.isNotBlank(type))
                                {
                                        addCategory(Category.getCategory(type), 7);
                                }
                            }
                        }};
                    }
                    String[] keyStringArray = keys;

                    Map<Category, List<SearchResult>> results = contentNameSearcher.search(queryTokens,resultTemplate,keyStringArray);//

                    List<Category> resultCategories = new ArrayList<Category>(results.keySet());

                    populateActionResult(results, CATEGORY_ORDERING);
                    resultCategories.removeAll(CATEGORY_ORDERING);

                    // if there are any additional categories left that have not been given an explicit ordering
                    // then they will be added to the end
                    populateActionResult(results, resultCategories);

                    result.addMatchGroup(createSearchAllGroupItem());
                }

                if (log.isDebugEnabled())
                {
                    log.debug("ContentNameSearch for term '" + query + "' took " + (System.currentTimeMillis() - startTime) + "ms");
                }
            }
            finally
            {
                permits.release();
            }
        }
        else
        {
            log.warn("A single quick nav search request could not be fulfilled since the limit of simultaneous quick nav searches has been reached. The search should be attempted again. Alternatively, consider adjusting this limit in General Configuration.");
            result.setStatusMessage(getText("quick.nav.server.busy"));
        }

		// always success - the ContentNameSearchResult will contain a statusMessage on error.
		return Action.SUCCESS;
	}

	/**
	 * Populate the action's result from the supplied searchResult parameter.
	 *
	 * @param searchResult the result to be used to populate the action's result
	 * @param categoryOrdering the categories to be populated and the order they should be considered
	 */
	private void populateActionResult(Map<Category, List<SearchResult>> searchResult, List<Category> categoryOrdering)
	{
		for (Category category : categoryOrdering)
		{
			List<SearchResult> categoryResults = searchResult.get(category);
			if ((categoryResults != null) && !categoryResults.isEmpty())
			{
				List<ContentNameMatch> matchesPerCategory = new ArrayList<ContentNameMatch>(categoryResults.size());
				for (SearchResult individualResult : categoryResults)
				{
                    ContentNameMatch match = createMatchFromSearchResult(individualResult);
					matchesPerCategory.add(match);
				}

				result.addMatchGroup(matchesPerCategory);
			}
		}
	}

	/**
	 * @return A ContentNameMatch created from the supplied SearchResult
	 */
	private ContentNameMatch createMatchFromSearchResult(SearchResult searchResult)
	{
		String iconUrl = null;
		if (searchResult.getCategory() == Category.PEOPLE)
			iconUrl = contentTypesDisplayMapper.getIconUrlForUsername(searchResult.getPreviewKey());

		String className = contentTypesDisplayMapper.getClassName(searchResult);

		String spaceName = searchResult.getSpaceName();
		if (!StringUtils.isEmpty(spaceName))
			spaceName = GeneralUtil.htmlEncode(spaceName);

		String spaceKey = searchResult.getSpaceKey();
		if (!StringUtils.isEmpty(spaceKey))
			spaceKey = GeneralUtil.htmlEncode(spaceKey);

        String contextPath = servletRequest.getContextPath();
        String iconPath = (iconUrl != null) ? contextPath + iconUrl : null;
        return new ContentNameMatch(className,  contextPath + searchResult.getUrl(), iconPath, GeneralUtil.htmlEncode(searchResult
				.getName()), spaceName, spaceKey);
	}

	private List<ContentNameMatch> createSearchAllGroupItem()
	{
        StringBuilder keyString = new StringBuilder();
        for (String key : keys)
        {
            keyString.append("&spaces=");
            keyString.append(key);
        }
        StringBuilder typeString = new StringBuilder();
        for (String t : types)
        {
            typeString.append("&type=");
            typeString.append(t);
        }

        String url = "/knowledgebase/kbsearchresults.action?where=" + spaceKey + keyString.toString() + typeString.toString() +"&queryString=" + GeneralUtil.urlEncode(query);
        String contextPath = servletRequest.getContextPath();
        ContentNameMatch searchAll = new ContentNameMatch(SEARCH_FOR_CSS_CLASS, contextPath + url,  null, getText(
				"contentnamesearch.search.for", new String[] { GeneralUtil.htmlEncode(query) }), null, null);

		return Collections.singletonList(searchAll);
	}

	public void setContentTypesDisplayMapper(ContentTypesDisplayMapper contentTypesIconMapper)
	{
		this.contentTypesDisplayMapper = contentTypesIconMapper;
	}

	public void setContentNameSearcher(ContentNameSearcher contentNameSearcher)
	{
		this.contentNameSearcher = contentNameSearcher;
	}

	public void setContentNameQueryTokenizer(QueryTokenizer contentNameQueryTokenizer)
	{
		this.contentNameQueryTokenizer = contentNameQueryTokenizer;
	}

	public void setQuery(String query)
	{
		this.query = query;
	}

    public void setType(String[] types)
    {
        this.types = (String[]) ArrayUtils.clone(types);
    }

    public void setSpaceKey(String spaceKey)
    {
        this.spaceKey = spaceKey;
    }

    public String getSpaceKey()
    {
        return spaceKey;
    }

    public void setKey(String keys[])
    {
        this.keys = (String[]) ArrayUtils.clone(keys);
    }

    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

	public ContentNameSearchResult getResult()
	{
		return result;
	}

    /**
	 *
	 * @return the result bean (<code>ContentNameSearchResult</code>) on successful execution of this action. If the
	 *         action has not executed then the return value will be null.
	 */
	public Object getBean()
	{
		return getResult();
	}

    public void setContentNameSearchSemaphoreHolder(SemaphoreHolder contentNameSearchSemaphoreHolder)
    {
        this.contentNameSearchSemaphoreHolder = contentNameSearchSemaphoreHolder;
    }

    public void setSettingsManager(SettingsManager settingsManager)
    {
        this.settingsManager = settingsManager;
    }

    public void setServletRequest(HttpServletRequest httpServletRequest)
    {
        this.servletRequest = httpServletRequest;
    }
}

