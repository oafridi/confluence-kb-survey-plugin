package com.atlassian.confluence.plugins.knowledgebase;

import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.plugin.descriptor.web.conditions.BaseConfluenceCondition;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import java.util.Collections;

public class SurveyCondition extends BaseConfluenceCondition
{
    
    private SpacePermissionManager spacePermissionManager;

    public boolean shouldDisplay(WebInterfaceContext context)
    {
        return spacePermissionManager.hasPermission(Collections.singletonList(SpacePermission.ADMINISTER_SPACE_PERMISSION),
                context.getSpace(), context.getUser());
    }

    public void setSpacePermissionManager(SpacePermissionManager spacePermissionManager)
    {
        this.spacePermissionManager = spacePermissionManager;
    }
}