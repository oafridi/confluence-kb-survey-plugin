package com.atlassian.confluence.plugins.knowledgebase.search;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.json.json.JsonArray;
import com.atlassian.confluence.json.json.JsonObject;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.themes.ThemeManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Map;

//This Macro only renders a search box that does quicksearch via AJS.
//For rendered velocity and java searching, it

//passes a queryString and a page ID to a parameter, then the destination page
//renders the search result (KbSearchResultsMacro)
public class KbSearchMacro extends BaseMacro
{
    private ThemeManager themeManager;
    private static String DOC_THEME_KEY = "com.atlassian.confluence.plugins.doctheme";

    public boolean isInline()
    {
        return true;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public void setThemeManager(ThemeManager themeManager)
    {
        this.themeManager = themeManager;
    }

    public String execute(Map params, String body, RenderContext renderContext) throws MacroException
    {
        PageContext pageContext;
        if (renderContext instanceof PageContext)
        {
            pageContext = (PageContext) renderContext;
        }
        else
        {
            throw new MacroException(ConfluenceActionSupport.getTextStatic("kbsearch.error.macro-works-in-only-page"));
        }

        if (!themeManager.getSpaceTheme(pageContext.getSpaceKey()).getPluginKey().equals(DOC_THEME_KEY))
        {
            throw new MacroException(ConfluenceActionSupport.getTextStatic("kbsearch.error.macro-works-in-only-doctheme"));
        }

        JsonObject spaceJsonObject = new JsonObject();
        JsonArray spaceJsonArray = new JsonArray();

        final Map<String, Object> contextMap = getMacroVelocityContext();
        Map<String, String> typeSafeParams = (Map<String, String>) params;
        String spaceParam;
        if (typeSafeParams.containsKey("spaces"))
        {
            spaceParam = typeSafeParams.get("spaces");
            String[] typesParamArray = spaceParam.split(",");
            for (String type : typesParamArray)
            {
                spaceJsonArray.add(nextSpacesJsonObject(type));
            }
        }
        else
        {
            spaceParam = pageContext.getSpaceKey();
        }
        spaceJsonObject.setProperty("spaces", spaceJsonArray);

        JsonObject typesJsonObject = new JsonObject();
        JsonArray typesJsonArray = new JsonArray();
        String typesParam;
        if (typeSafeParams.containsKey("types"))
        {
            typesParam = typeSafeParams.get("types");
            String[] typesParamArray = typesParam.split(",");
            for (String type : typesParamArray)
            {
                typesJsonArray.add(nextTypesJsonObject(type));
            }
        }
        else
        {
            typesParam = "pages,blogposts,attachments";
        }
        typesJsonObject.setProperty("types", typesJsonArray);

        contextMap.put("typesJson", GeneralUtil.urlEncode(typesJsonObject.serialize()));
        contextMap.put("spacesJson", GeneralUtil.urlEncode(spaceJsonObject.serialize()));
        contextMap.put("spaces", spaceParam.split(","));
        contextMap.put("types", typesParam.split(","));
        contextMap.put("spaceKey", pageContext.getSpaceKey());
        return VelocityUtils.getRenderedTemplate("com/atlassian/confluence/plugins/knowledgebase/search/kbsearchmacro.vm", contextMap);
    }

    private JsonObject nextTypesJsonObject(String type)
    {
        JsonObject nextDayDateJsonObject = new JsonObject();
        nextDayDateJsonObject.setProperty("type", type);

        return nextDayDateJsonObject;
    }

    private JsonObject nextSpacesJsonObject(String space)
    {
        JsonObject nextDayDateJsonObject = new JsonObject();
        nextDayDateJsonObject.setProperty("space", space);

        return nextDayDateJsonObject;
    }

    protected Map<String, Object> getMacroVelocityContext()
    {
        return MacroUtils.defaultVelocityContext();
    }
}
