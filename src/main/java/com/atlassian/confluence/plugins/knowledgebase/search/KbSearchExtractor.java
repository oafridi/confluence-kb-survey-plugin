package com.atlassian.confluence.plugins.knowledgebase.search;

import static com.atlassian.confluence.plugins.knowledgebase.Constants.COMPOSITE_SCORE;

import com.atlassian.bonnie.Searchable;
import com.atlassian.bonnie.search.Extractor;
import com.atlassian.confluence.core.ConfluencePropertySetManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.SpaceManager;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

public class KbSearchExtractor implements Extractor{
    private ContentPropertyManager contentPropertyManager;
    public static final String COMPOSITE_SCORE_FIELD_KEY = "compositeScore";
    private SpaceManager spaceManager;
    private ConfluencePropertySetManager propertySetManager;
    private static Logger log = Logger.getLogger(KbSearchExtractor.class);

    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager)
    {
        this.contentPropertyManager = contentPropertyManager;
    }

    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public void setPropertySetManager(ConfluencePropertySetManager propertySetManager)
    {
        this.propertySetManager = propertySetManager;
    }

    public void addFields(Document document, StringBuffer not_used, Searchable searchable)
    {

        if (!(searchable instanceof ContentEntityObject))
        {
            return;
        }

        ContentEntityObject content = (ContentEntityObject) searchable;
        if (StringUtils.isBlank(content.getTitle()))
        {
            return;
        }

        if (content instanceof Page)
        {
            String entityKey = COMPOSITE_SCORE + spaceManager.getSpaceFromPageId(content.getId());
            if (propertySetManager.getPropertySet(content).getInt(entityKey)!=0)
                {
                    final Field field = new Field(
                    COMPOSITE_SCORE_FIELD_KEY,
                    Integer.toString(propertySetManager.getPropertySet(content).getInt(entityKey)), Field.Store.YES, Field.Index.NOT_ANALYZED);
                    document.add(field);
                }
        }

    }

}
