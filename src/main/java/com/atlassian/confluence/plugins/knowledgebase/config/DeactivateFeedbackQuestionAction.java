package com.atlassian.confluence.plugins.knowledgebase.config;

import com.atlassian.confluence.spaces.actions.AbstractSpaceAdminAction;
import com.atlassian.confluence.spaces.actions.SpaceAware;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;

@WebSudoRequired
public class DeactivateFeedbackQuestionAction extends AbstractSpaceAdminAction implements SpaceAware
{
    private String selectedQuestion;
    private boolean activate;
    private SurveyFeedbackManager surveyFeedbackManager;

    public String execute() throws Exception
    {
        SpaceDescription spaceDesc = this.getSpace().getDescription();
        SurveyConfig surveyConfig = surveyFeedbackManager.getSurveyConfig(spaceDesc);

        if (activate)
            surveyConfig.activateQuestion(selectedQuestion);
        else
            surveyConfig.deactivateQuestion(selectedQuestion);
        surveyFeedbackManager.setSurveyConfig(spaceDesc,surveyConfig);

        return super.doDefault();
    }

    public void setSelectedQuestion(String questionId)
    {
        selectedQuestion = questionId;
    }

    public void setActivate(String activateString)
    {
        activate = activateString.equals("true");
    }

    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }
 
}
