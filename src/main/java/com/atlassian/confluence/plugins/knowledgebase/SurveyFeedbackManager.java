package com.atlassian.confluence.plugins.knowledgebase;

import com.atlassian.confluence.plugins.knowledgebase.config.SurveyConfig;
import com.atlassian.confluence.plugins.knowledgebase.results.SurveyGaResult;
import com.atlassian.confluence.plugins.knowledgebase.results.SurveyResult;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;

import java.util.List;
import java.util.Date;


public interface SurveyFeedbackManager {
    public SurveyConfig getSurveyConfig(SpaceDescription spaceDesc);

    public void setSurveyConfig(SpaceDescription spaceDesc, SurveyConfig surveyConfig);

    public String getCompositeScore(AbstractPage page);
    public String getCompositeScore(String gaTag, String gaValue, String spaceKey);

    public String getNumberOfSurveys(AbstractPage page, String spaceKey);
    public String getNumberOfSurveys(String gaTag, String gaValue, String spaceKey);

    public String getYesNoPercentage(AbstractPage page, String questionId);
    public String getYesNoPercentage(String gaTag, String gaValue, String spaceKey, String questionId);

    public String getSpaceBallotCount(Space space);

    public String getSpacePrimaryYesNoTotal(String questionId);

    public void addSurveyResult(AbstractPage page, String questionId, Boolean answer, String spaceKey, String utmSource, String utmMedium, String utmContent);
    public void addSurveyResult(AbstractPage page, String questionId, Boolean answer, String spaceKey, String utmSource, String utmMedium, String utmContent, Date date);

    public List<String> getPageIdsSortedByCompositeScore(String spaceKey, Integer max);

    public List<Long> getUnsurveyedPageIdList(Space space, PageManager pageManager);

    public int getNumberOfSurveyedPages(String spaceKey);
    public int getNumberOfGaValues(String gaTag, String spaceKey);

    public List<SurveyResult> getPagedResults(String order, String spaceKey, int offset, int max);
    public List<SurveyGaResult> getPagedResults(String gaTag, String order, String spaceKey, int offset, int max);

    public void evictSurveyResults(AbstractPage page);

    public boolean hasPageResult(AbstractPage page, String spaceKey);
    public boolean hasGaResult(String gaTag, String spaceKey);
}
