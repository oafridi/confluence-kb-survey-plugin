package com.atlassian.confluence.plugins.knowledgebase.survey;

import java.util.Map;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.opensymphony.webwork.ServletActionContext;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.confluence.plugins.knowledgebase.config.SurveyConfig;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class KbSurveyMacro extends BaseMacro
{
    private SpaceManager spaceManager;
    private PageManager pageManager;
    private String pageId;
    private AbstractPage page;
    private ConfluenceActionSupport confluenceActionSupport;
    private static Logger log = Logger.getLogger(KbSurveyMacro.class);
    private HttpSession session;
    private SurveyConfig surveyConfig;
    private SurveyFeedbackManager surveyFeedbackManager;

    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }

    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    protected ConfluenceActionSupport getConfluenceActionSupport()
    {
        if (null == confluenceActionSupport)
        {
            confluenceActionSupport = GeneralUtil.newWiredConfluenceActionSupport();
        }
        return confluenceActionSupport;
    }

    public String execute(Map params, String body, RenderContext renderContext) throws MacroException
    {
        final HttpServletRequest request = ServletActionContext.getRequest();
        session = request.getSession();

        PageContext pageContext;

        if (!(renderContext instanceof PageContext) || !((((PageContext) renderContext).getEntity() instanceof com.atlassian.confluence.pages.Page)))
        {
            throw new MacroException(ConfluenceActionSupport.getTextStatic("kbsearch.error.macro-works-in-only-page"));
        }

        if (renderContext.getOutputType().equals("preview"))
        {
            throw new MacroException(ConfluenceActionSupport.getTextStatic("kbsearch.error.macro-works-in-non-preview-mode"));
        }

        pageContext = (PageContext) renderContext;
        String spaceKey = pageContext.getSpaceKey();

        //redirectURL = ;

        Space space = spaceManager.getSpace(spaceKey);
        page = pageManager.getPage(spaceKey, pageContext.getPageTitle());
        pageId = page.getIdAsString();

        SpaceDescription spaceDesc = space.getDescription();

        surveyConfig = surveyFeedbackManager.getSurveyConfig(spaceDesc);
        if (null != request.getParameter("surveyCompleted"))
        {
            return surveyConfig.getResponse();
        }// "Thanks for submitting your feedback.";

        Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
        contextMap.put("macro", this);
        contextMap.put("surveyConfig", surveyConfig);
        return VelocityUtils.getRenderedTemplate("/com/atlassian/confluence/plugins/knowledgebase/survey/kbsurvey.vm", contextMap);
    }

    public boolean isQuestionAnswered(String questionId)
    {
        return session.getAttribute(KbHttpSession.getSessionAttribute(page, questionId)) != null;
    }

    public boolean isPrimaryQuestionAnswered(String questionId)
    {
        return session.getAttribute(KbHttpSession.getSessionAttribute(page, questionId)) != null;
    }

    public void doEvictQuestions()
    {
        boolean removeAll = true;

        for (String questionId : surveyConfig.getQuestionMap().keySet())
        {
            log.debug("questionId: " + pageId + questionId + ":" + session.getAttribute(KbHttpSession.getSessionAttribute(page, questionId)));
            log.debug("removeAll: " + removeAll);
            if (session.getAttribute(KbHttpSession.getSessionAttribute(page, questionId)) == null)
            {
                removeAll = false;
                break;
            }
        }

        if (isPrimaryQuestionAnswered(surveyConfig.getPrimaryQuestionId()) && surveyConfig.getActiveQuestionList().isEmpty())
        {
            removeAll = true;
        }

        if (removeAll)
        {
            log.debug("Removing all");
            for (String questionId2 : surveyConfig.getQuestionMap().keySet())
            {
                session.removeAttribute(KbHttpSession.getSessionAttribute(page, questionId2));
            }
            session.removeAttribute(KbHttpSession.getSessionAttribute(page, surveyConfig.getPrimaryQuestionId()));
        }
    }

    public boolean isLatestVersionRequired()
    {
        return true;
    }

    public boolean isPageRequired()
    {
        return true;
    }

    public boolean isViewPermissionRequired()
    {
        return true;
    }

}

