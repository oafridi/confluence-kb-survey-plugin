package com.atlassian.confluence.plugins.knowledgebase.results;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.actions.AbstractPageAwareAction;
import com.atlassian.confluence.pages.actions.ActionHelper;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.opensymphony.webwork.ServletActionContext;

@WebSudoRequired
public class EvictSurveyResultsAction extends AbstractPageAwareAction
{
    private PageManager pageManager;
    private long pageId;
    private String redirectUrl;
    private SurveyFeedbackManager surveyFeedbackManager;

    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }

    public String doRemove()
    {
        surveyFeedbackManager.evictSurveyResults(pageManager.getPage(pageId));
        return SUCCESS;
    }

    public String execute() throws Exception
    {
        setRedirectUrl();
        return super.execute();
    }

    public String getNumberOfSurveys(AbstractPage page)
    {
        return surveyFeedbackManager.getNumberOfSurveys(page,spaceManager.getSpaceFromPageId(page.getId()));
    }

    public String getCompositeScore(AbstractPage page)
    {
        return surveyFeedbackManager.getCompositeScore(page);
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public String doDefault() throws Exception
    {
       return super.doDefault();
    }

    public AbstractPage getPage()
    {
        return super.getPage();
    }

    public boolean isPermitted()
    {
        return (isSpaceAdmin()) && super.isPermitted();
    }

    public void setRedirectUrl()
    {
        redirectUrl = ServletActionContext.getRequest().getHeader("Referer");
    }

    public String getRedirectUrl()
    {
        return redirectUrl;
    }

    private boolean isSpaceAdmin()
    {
        return ActionHelper.isSpaceAdmin(getSpace(),getRemoteUser(), spacePermissionManager);
    }

    public void setPageId(long pageId)
    {
        this.pageId = pageId;
    }
}




