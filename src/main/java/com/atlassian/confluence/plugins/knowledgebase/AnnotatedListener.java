package com.atlassian.confluence.plugins.knowledgebase;

import net.java.ao.Query;

import org.springframework.beans.factory.DisposableBean;

import static com.google.common.base.Preconditions.checkNotNull;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.event.events.content.page.PageRemoveEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;

public class AnnotatedListener implements DisposableBean {
	protected EventPublisher eventPublisher;
	private ActiveObjects ao;
	
    public AnnotatedListener(EventPublisher eventPublisher, ActiveObjects ao) {
    	this.ao = checkNotNull(ao);
        this.eventPublisher = eventPublisher;
        eventPublisher.register(this);
    }
    
    /* When deleting an space, a pageRemoveEvent is generated for every page, so no need to listen for spaceRemoveEvent
     * until space settings are stored using Active Objects (https://studio.plugins.atlassian.com/browse/CKBSP-45)
     */
    @EventListener
    public void pageRemoveEvent(PageRemoveEvent event) {
		Response[] rowsToDelete = ao.find(Response.class, Query.select().where("PAGE_ID = ?", event.getPage().getId()));
		ao.delete(rowsToDelete);
    }
    
	public void destroy() throws Exception {
    	eventPublisher.unregister(this);
	}

}
