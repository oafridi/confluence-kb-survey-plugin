package com.atlassian.confluence.plugins.knowledgebase.survey;

import com.atlassian.confluence.pages.AbstractPage;

public class KbHttpSession
{
    private static final String KB_SESSION_ATT = "kb-survey";

    public static String getSessionAttribute(AbstractPage page, String qId)
    {
        return KB_SESSION_ATT + page.getIdAsString() + qId;
    }

}
